import numpy as np
import os
from scipy import interpolate
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class Vehicle:

    class Aircraft(object):

        def __init__(self, aircraftProp):

            self.totalMass                  = aircraftProp["Mass"]
            self.fuelMass                   = aircraftProp["fuelMass"]
            self.initFuelMass               = self.fuelMass
            self.fuelFraction               = self.fuelMass / self.initFuelMass
            self.emptyMass                  = self.totalMass - self.fuelMass
            self.planformArea               = aircraftProp["planformArea"]
            self.maxG                       = aircraftProp["maxG"]
            self.intakeAreaRatio            = aircraftProp["intakeAreaRatio"]
            self.totalWeight                = self.totalMass * 9.81

        def update(self, consumedFuel):

            self.fuelMass                   = self.fuelMass - consumedFuel
            self.fuelFraction               = self.fuelMass / self.initFuelMass
            self.totalMass                  = self.totalMass - consumedFuel
            self.totalWeight                = self.totalMass * 9.81

    class Engine:

        class LookUpTable(object):

            def __init__(self, filename, intakeAreaRatio):

                self.filename               = filename
                self.intakeAreaRatio        = intakeAreaRatio

                self.read()

            def read(self):

                with open(os.path.join(os.path.dirname(os.path.realpath("__file__")), self.filename), "r") as output:

                    outputLines                 = output.readlines()
                    outputWords                 = [outputLines[i].split() for i in range(len(outputLines))]

                    self.altitudeLow            = [float(outputWords[i][0]) for i in range(len(outputLines)) if i % 2 == 0]
                    self.throttleLow            = [float(outputWords[i][1]) for i in range(len(outputLines)) if i % 2 == 0]
                    self.fuelLow                = [float(outputWords[i][2]) for i in range(len(outputLines)) if i % 2 == 0]
                    self.engineForceLow         = [float(outputWords[i][3]) for i in range(len(outputLines)) if i % 2 == 0]
                    self.exhaustVelocityLow     = [float(outputWords[i][4]) for i in range(len(outputLines)) if i % 2 == 0]
                    self.airFlowLow             = [float(outputWords[i][5]) for i in range(len(outputLines)) if i % 2 == 0]

                    self.altitudeHigh           = [float(outputWords[i][0]) for i in range(len(outputLines)) if i % 2 == 1]
                    self.throttleHigh           = [float(outputWords[i][1]) for i in range(len(outputLines)) if i % 2 == 1]
                    self.fuelHigh               = [float(outputWords[i][2]) for i in range(len(outputLines)) if i % 2 == 1]
                    self.engineForceHigh        = [float(outputWords[i][3]) for i in range(len(outputLines)) if i % 2 == 1]
                    self.exhaustVelocityHigh    = [float(outputWords[i][4]) for i in range(len(outputLines)) if i % 2 == 1]
                    self.airFlowHigh            = [float(outputWords[i][5]) for i in range(len(outputLines)) if i % 2 == 1]

            def fullThrottle(self, altitude, flightVelocity):

                self.throttle               = 1.

                altitudeValues              = [self.altitudeLow[len(self.altitudeLow) - 1],                 self.altitudeHigh[len(self.altitudeHigh) - 1]]
                fuelValues                  = [self.fuelLow[len(self.fuelLow) - 1],                         self.fuelHigh[len(self.fuelHigh) - 1]]
                engineForceValues           = [self.engineForceLow[len(self.engineForceLow) - 1],           self.engineForceHigh[len(self.engineForceHigh) - 1]]
                exhaustVelocityValues       = [self.exhaustVelocityLow[len(self.exhaustVelocityLow) - 1],   self.exhaustVelocityHigh[len(self.exhaustVelocityHigh) - 1]]
                airFlowValues               = [self.airFlowLow[len(self.airFlowLow) - 1],                   self.airFlowHigh[len(self.airFlowHigh) - 1]]

                if altitude > self.altitudeHigh[0]:

                    interpolateFuel             = interpolate.interp1d(altitudeValues, fuelValues, fill_value='extrapolate')
                    interpolateForce            = interpolate.interp1d(altitudeValues, engineForceValues, fill_value='extrapolate')
                    interpolateVelocity         = interpolate.interp1d(altitudeValues, exhaustVelocityValues, fill_value='extrapolate')
                    interpolateAir              = interpolate.interp1d(altitudeValues, airFlowValues, fill_value='extrapolate')

                else:

                    interpolateFuel             = interpolate.interp1d(altitudeValues, fuelValues, fill_value='interpolate')
                    interpolateForce            = interpolate.interp1d(altitudeValues, engineForceValues, fill_value='interpolate')
                    interpolateVelocity         = interpolate.interp1d(altitudeValues, exhaustVelocityValues, fill_value='interpolate')
                    interpolateAir              = interpolate.interp1d(altitudeValues, airFlowValues, fill_value='interpolate')

                self.fuel                       = interpolateFuel(altitude) / 60. / 1000.       # kg / sec
                self.engineForce                = interpolateForce(altitude)                    # N
                self.exhaustVelocity            = interpolateVelocity(altitude)                 # m / sec
                self.airflowRate                = interpolateAir(altitude)                      # kg / sec

                self.currentThrust              = self.airflowRate * (self.exhaustVelocity - flightVelocity / self.intakeAreaRatio)

            def fixedThrottle(self, throttle, altitude, flightVelocity):

                self.throttle                   = throttle

                fuels                           = []
                engineForces                    = []
                exhaustVelocities               = []
                airflowRates                    = []

                for i in range(len(self.altitudeLow)):

                    altitudeValues              = [self.altitudeLow[i],                 self.altitudeHigh[i]]
                    fuelValues                  = [self.fuelLow[i],                         self.fuelHigh[i]]
                    engineForceValues           = [self.engineForceLow[i],           self.engineForceHigh[i]]
                    exhaustVelocityValues       = [self.exhaustVelocityLow[i],   self.exhaustVelocityHigh[i]]
                    airFlowValues               = [self.airFlowLow[i],                   self.airFlowHigh[i]]

                    if altitude > self.altitudeHigh[i]:

                        interpolateFuel             = interpolate.interp1d(altitudeValues, fuelValues, fill_value='extrapolate')
                        interpolateForce            = interpolate.interp1d(altitudeValues, engineForceValues, fill_value='extrapolate')
                        interpolateVelocity         = interpolate.interp1d(altitudeValues, exhaustVelocityValues, fill_value='extrapolate')
                        interpolateAir              = interpolate.interp1d(altitudeValues, airFlowValues, fill_value='extrapolate')

                    else:

                        interpolateFuel             = interpolate.interp1d(altitudeValues, fuelValues, fill_value='interpolate')
                        interpolateForce            = interpolate.interp1d(altitudeValues, engineForceValues, fill_value='interpolate')
                        interpolateVelocity         = interpolate.interp1d(altitudeValues, exhaustVelocityValues, fill_value='interpolate')
                        interpolateAir              = interpolate.interp1d(altitudeValues, airFlowValues, fill_value='interpolate')

                    fuels.append(interpolateFuel(altitude))
                    engineForces.append(interpolateForce(altitude))
                    exhaustVelocities.append(interpolateVelocity(altitude))
                    airflowRates.append(interpolateAir(altitude))

                interpolateFuel                     = interpolate.interp1d(self.throttleLow, fuels, fill_value='interpolate')
                interpolateForce                    = interpolate.interp1d(self.throttleLow, engineForces, fill_value='interpolate')
                interpolateVelocity                 = interpolate.interp1d(self.throttleLow, exhaustVelocities, fill_value='interpolate')
                interpolateAir                      = interpolate.interp1d(self.throttleLow, airflowRates, fill_value='interpolate')

                self.fuel                           = interpolateFuel(self.throttle) / 60. / 1000.       # kg / sec
                self.engineForce                    = interpolateForce(self.throttle)                    # N
                self.exhaustVelocity                = interpolateVelocity(self.throttle)                 # m / sec
                self.airflowRate                    = interpolateAir(self.throttle)                      # kg / sec

                self.currentThrust                  = self.airflowRate * (self.exhaustVelocity - flightVelocity / self.intakeAreaRatio)

            def fixedVelocity(self, drag, altitude, flightVelocity):

                fuels                           = []
                thrusts                         = []
                exhaustVelocities               = []
                airflowRates                    = []

                for i in range(len(self.altitudeLow)):

                    altitudeValues              = [self.altitudeLow[i],                 self.altitudeHigh[i]]
                    fuelValues                  = [self.fuelLow[i],                         self.fuelHigh[i]]
                    exhaustVelocityValues       = [self.exhaustVelocityLow[i],   self.exhaustVelocityHigh[i]]
                    airFlowValues               = [self.airFlowLow[i],                   self.airFlowHigh[i]]

                    if altitude > self.altitudeHigh[i]:

                        interpolateFuel             = interpolate.interp1d(altitudeValues, fuelValues, fill_value='extrapolate')
                        interpolateVelocity         = interpolate.interp1d(altitudeValues, exhaustVelocityValues, fill_value='extrapolate')
                        interpolateAir              = interpolate.interp1d(altitudeValues, airFlowValues, fill_value='extrapolate')

                    else:

                        interpolateFuel             = interpolate.interp1d(altitudeValues, fuelValues, fill_value='interpolate')
                        interpolateVelocity         = interpolate.interp1d(altitudeValues, exhaustVelocityValues, fill_value='interpolate')
                        interpolateAir              = interpolate.interp1d(altitudeValues, airFlowValues, fill_value='interpolate')

                    fuels.append(interpolateFuel(altitude))
                    exhaustVelocities.append(interpolateVelocity(altitude))
                    airflowRates.append(interpolateAir(altitude))
                    thrusts.append(interpolateAir(altitude)* (interpolateVelocity(altitude) - flightVelocity / self.intakeAreaRatio))

                interpolateFuel                     = interpolate.interp1d(thrusts, fuels, fill_value='interpolate')
                interpolateVelocity                 = interpolate.interp1d(thrusts, exhaustVelocities, fill_value='interpolate')
                interpolateAir                      = interpolate.interp1d(thrusts, airflowRates, fill_value='interpolate')
                interpolateThrottle                 = interpolate.interp1d(thrusts, self.throttleLow, fill_value='interpolate')

                self.fuel                           = interpolateFuel(drag) / 60. / 1000.       # kg / sec
                self.exhaustVelocity                = interpolateVelocity(drag)                 # m / sec
                self.airflowRate                    = interpolateAir(drag)                      # kg / sec

                self.throttle                       = interpolateThrottle(drag)

                self.currentThrust                  = drag

        class DECK(object):
            pass

        class Model(object):
            pass

    class Database:

        class LookUpTable(object):      # Lift Force1 must be smaller than, Lift Force2 must be bigger than Weight!!!!

            def __init__(self, filename, databaseRho):

                self.filename               = filename
                self.dbRho                  = databaseRho

                self.read()
                self.rotate()

            def read(self):

                with open(os.path.join(os.path.dirname(os.path.realpath("__file__")), self.filename), "r") as output:

                    outputLines             = output.readlines()
                    outputWords             = [outputLines[i].split() for i in range(len(outputLines))]

                    self.Mach               = [float(outputWords[i][0]) for i in range(len(outputLines)) if i%2 == 1]
                    self.AoA                = [float(outputWords[i][2]) for i in range(len(outputLines))]
                    self.Fx                 = [float(outputWords[i][3]) for i in range(len(outputLines))]
                    self.Fz                 = [float(outputWords[i][5]) for i in range(len(outputLines))]

            def rotate(self):

                self.lift                   = self.Fz * np.cos(np.radians(self.AoA)) - self.Fx * np.sin(np.radians(self.AoA))
                self.drag                   = self.Fz * np.sin(np.radians(self.AoA)) + self.Fx * np.cos(np.radians(self.AoA))

                self.liftLow                = [self.lift[i] for i in range(len(self.lift)) if i%2 == 0]
                self.liftHigh               = [self.lift[i] for i in range(len(self.lift)) if i%2 == 1]

                self.dragLow                = [self.drag[i] for i in range(len(self.lift)) if i%2 == 0]
                self.dragHigh               = [self.drag[i] for i in range(len(self.lift)) if i%2 == 1]

            def interpolateAltitude(self, currentRho):      # Forces at current altitude

                self.liftNormalized         = [self.lift[i]     * currentRho / self.dbRho for i in range(len(self.lift))]
                self.liftLowNormalized      = [self.liftLow[i]  * currentRho / self.dbRho for i in range(len(self.liftLow))]
                self.liftHighNormalized     = [self.liftHigh[i] * currentRho / self.dbRho for i in range(len(self.liftHigh))]

                self.dragNormalized         = [self.drag[i]     * currentRho / self.dbRho for i in range(len(self.drag))]
                self.dragLowNormalized      = [self.dragLow[i]  * currentRho / self.dbRho for i in range(len(self.dragLow))]
                self.dragHighNormalized     = [self.dragHigh[i] * currentRho / self.dbRho for i in range(len(self.dragHigh))]

            def interpolateWeight(self, aircraftWeight):        # Drag forces for current weight

                self.interpolatedDrag       = []

                for i in range(len(self.liftLow)):

                    lift_values             = [self.liftLowNormalized[i], self.liftHighNormalized[i]]
                    drag_values             = [self.dragLowNormalized[i], self.dragHighNormalized[i]]

                    if aircraftWeight < self.liftLowNormalized[i] or aircraftWeight > self.liftHighNormalized[i]:

                        interpolationFunc   = interpolate.interp1d(lift_values, drag_values, fill_value='extrapolate')

                    else:

                        interpolationFunc   = interpolate.interp1d(lift_values, drag_values, fill_value='interpolate')

                    drag                    = interpolationFunc(aircraftWeight)

                    self.interpolatedDrag.append(drag)

            def interpolateMach(self, mach):                # Drag FORCE for current mach

                if mach < self.Mach[0] or mach > self.Mach[len(self.Mach) - 1]:

                    interpolationFunc           = interpolate.interp1d(self.Mach, self.interpolatedDrag, fill_value='extrapolate')

                else:

                    interpolationFunc           = interpolate.interp1d(self.Mach, self.interpolatedDrag, fill_value='interpolate')

                self.currentDragForce       = float(interpolationFunc(mach))

            def dragForce(self, currentMach, currentAircraftWeight, currentRho):

                self.interpolateAltitude(currentRho)
                self.interpolateWeight(currentAircraftWeight)
                self.interpolateMach(currentMach)

        class DATCOM(object):
            pass

        class PANEL(object):
            pass

        class CFD(object):
            pass

class Conditions:

    class InitialConditions(object):

        def __init__(self, initialConditions):

            self.initialMach                = initialConditions["initialMach"]
            self.machIncrement              = initialConditions["machIncrement"]
            self.timeIncrement              = initialConditions["timeIncrement"]

    class AtmosphericConditions(object):

        def __init__(self, atmosphere):

            self.altitude                   = atmosphere["altitude"]
            self.altitudeFT                 = self.altitude / 0.3048
            self.ISA                        = atmosphere["ISA"]
            self.gamma                      = atmosphere["gamma"]
            self.R                          = atmosphere["R"]
            self.gravity                    = 9.81
            self.seaLevelPressure           = 101300.

            if self.altitudeFT > 36089.:

                self.temperature            = self.ISA - 56.5 + 273.15
                self.pressure               = 0.22336 * np.exp(-0.0000480634 * (self.altitudeFT - 36089)) * self.seaLevelPressure

            else:

                self.temperature            = (1 - 0.00000687535 * self.altitudeFT) * 288.15 + self.ISA
                self.pressure               = pow((1 - 0.00000687535 * self.altitudeFT), 5.2561) * self.seaLevelPressure

            self.density                    = self.pressure / self.temperature / self.R
            self.soundSpeed                 = np.sqrt(self.gamma * self.R * self.temperature)

        def update(self, newAltitude):

            self.altitude                   = newAltitude
            self.altitudeFT                 = self.altitude / 0.3048
            self.ISA                        = self.ISA
            self.gamma                      = self.gamma
            self.R                          = self.R
            self.seaLevelPressure           = 101300.

            if self.altitudeFT > 36089.:

                self.temperature            = self.ISA - 56.5 + 273.15
                self.pressure               = 0.22336 * np.exp(-0.0000480634 * (self.altitudeFT - 36089)) * self.seaLevelPressure

            else:

                self.temperature            = (1 - 0.00000687535 * self.altitudeFT) * 288.15 + self.ISA
                self.pressure               = pow((1 - 0.00000687535 * self.altitudeFT), 5.2561) * self.seaLevelPressure

            self.density                    = self.pressure / self.temperature / self.R
            self.soundSpeed                 = np.sqrt(self.gamma * self.R * self.temperature)

    class StopConditions(object):

        def __init__(self, stopConditions):

            self.moveType       = stopConditions["move"]
            self.speedType      = stopConditions["speed"]
            self.constraint     = stopConditions["constraint"]
            self.value          = stopConditions["value"]
            self.setThrottle    = stopConditions["throttle"]

class FlightMechanics(object):

    def __init__(self, aircraft, atmosphere, initialConditions):

        self.moveType                       = 0
        self.speedType                      = 0

        self.maxG                           = aircraft.maxG
        self.mass                           = aircraft.totalMass

        self.altitude                       = atmosphere.altitude
        self.gravity                        = atmosphere.gravity
        self.sound                          = atmosphere.soundSpeed

        self.mach                           = initialConditions.initialMach
        self.increment                      = initialConditions.machIncrement
        self.timeIncrement                  = initialConditions.timeIncrement

        self.flightTime                     = 0.
        self.flightSpeed                    = self.mach * self.sound

        self.x                              =   0.
        self.y                              =   0.
        self.z                              = - self.altitude

        self.i                              = 1
        self.j                              = 0
        self.k                              = 0

    def nextTime(self, aircraft, atmosphere, databaseDrag, engineThrust):

        self.sound                          = atmosphere.soundSpeed
        self.flightSpeed                    = self.sound * self.mach
        self.altitude                       = - self.z

        self.liftForce()
        self.dragForce(databaseDrag)
        self.netForce(engineThrust)
        self.netTime()
        self.turnRadius()
        self.turnAngle()

        self.flightVector()
        self.flightTrajectory()

        self.mach       = self.mach
        self.mass       = aircraft.totalMass
        self.flightTime = self.flightTime + self.time

    def nextMach(self, aircraft, atmosphere, databaseDrag, engineThrust):

        self.sound                          = atmosphere.soundSpeed
        self.mass                           = aircraft.totalMass
        self.flightSpeed                    = self.sound * self.mach
        self.altitude                       = - self.z

        self.liftForce()
        self.dragForce(databaseDrag)
        self.netForce(engineThrust)
        self.netTime()
        self.turnRadius()
        self.turnAngle()

        self.flightVector()
        self.flightTrajectory()

        if self.speedType   == 1:

            self.mach       = self.mach    + self.increment

        elif self.speedType == -1:

            self.mach       = self.mach    - self.increment

        self.flightTime     = self.flightTime + self.time

    def liftForce(self):

        if self.moveType    == 2:

            self.lift = pow((pow((self.mass * self.gravity), 2.) + (pow((self.mass * self.maxG), 2.))), 0.5)

        else:

            self.lift   = self.mass * self.gravity * np.abs(self.i)

    def dragForce(self, databaseDrag):

        if self.moveType    == 2:

            self.drag   = databaseDrag

        else:
            self.drag   = databaseDrag - self.mass * self.gravity * self.k

    def netForce(self, engineThrust):

        if self.speedType   == 0:

            self.net    = 0.

        else:

            self.net    = engineThrust - self.drag

    def netTime(self):

        if self.speedType   == 1:

            self.time   = self.increment * self.sound * self.mass / self.net

        elif self.speedType   == -1:

            self.time   = - self.increment * self.sound * self.mass / self.net

        elif self.speedType   == 0:

            self.time   = self.timeIncrement

    def turnRadius(self):

        self.radius  = self.mach * self.mach * self.sound * self.sound / self.maxG

    def turnAngle(self):

        self.angle  = np.degrees(np.arctan(self.time * self.mach * self.sound / self.radius))

    def flightVector(self):

        if self.moveType    == 0:

            self.i      = self.i
            self.j      = self.j
            self.k      = self.k

        elif self.moveType  == 2:

            self.i  = self.i * np.cos(np.radians(self.angle)) + self.j * np.sin(np.radians(self.angle))
            self.j  = self.j * np.cos(np.radians(self.angle)) - self.i * np.sin(np.radians(self.angle))
            self.k  = self.k

        elif self.moveType  == 1:

            self.i  = self.i * np.cos(np.radians(self.angle)) + self.k * np.sin(np.radians(self.angle))
            self.j  = self.j
            self.k  = self.k * np.cos(np.radians(self.angle)) - self.i * np.sin(np.radians(self.angle))

        elif self.moveType  == -1:

            self.i  = self.i * np.cos(np.radians(self.angle)) - self.k * np.sin(np.radians(self.angle))
            self.j  = self.j
            self.k  = self.k * np.cos(np.radians(self.angle)) + self.i * np.sin(np.radians(self.angle))

    def aircraftVector(self):
        pass

    def flightTrajectory(self):

        self.x          = self.x + self.i * (self.time * self.mach * self.sound + self.time * self.time * self.net / self.mass / 2.)
        self.y          = self.y + self.j * (self.time * self.mach * self.sound + self.time * self.time * self.net / self.mass / 2.)
        self.z          = self.z + self.k * (self.time * self.mach * self.sound + self.time * self.time * self.net / self.mass / 2.)

class Driver(object):

    def __init__(self, aircraft, engine, database, atmosphere, flightMechanics, initialConditions, stopSonditions):

        self.results            = {"x": [], "y": [], "z": [], "i": [], "j": [], "k": [], "mach": [], "time": [],
                                   "fuelFraction": [], "dbDrag": [], "acDrag": [], "thrust": [], "netForce": [],
                                   "throttle": [], "fuelRate":[]}

        self.iterScenario       = 0

        self.AC                 = aircraft
        self.EN                 = engine
        self.DB                 = database

        self.FC                 = initialConditions
        self.AP                 = atmosphere
        self.SC                 = stopSonditions

        self.FM                 = flightMechanics

        # SET SOLVER STRUCTURE
        if self.SC.speedType[self.iterScenario] == 0:
            self.structure          = 1
            self.FM.speedType       = 0
        elif self.SC.speedType[self.iterScenario]   == 1:
            self.structure          = 2
            self.FM.speedType       = 1
        else:
            self.structure          = 2
            self.FM.speedType       = -1

        # SET PILOT INPUT
        if self.SC.moveType[self.iterScenario]      == -1:
            self.FM.moveType           = -1
        elif self.SC.moveType[self.iterScenario]    == 0:
            self.FM.moveType           = 0
        elif self.SC.moveType[self.iterScenario]    == 1:
            self.FM.moveType           = 1
        else:
            self.FM.moveType           = 2

        # SET STOP TYPE
        if self.SC.constraint[self.iterScenario]    == 1:
            self.stopConstraint     = 1
        elif self.SC.constraint[self.iterScenario]  == 2:
            self.stopConstraint     = 2
        elif self.SC.constraint[self.iterScenario]  == 3:
            self.stopConstraint     = 3
        elif self.SC.constraint[self.iterScenario]  == 4:
            self.stopConstraint     = 4
        else:
            self.stopConstraint     = 5


        # SET STOP VALUE
        self.stopValue              = self.SC.value[self.iterScenario]

        # SET THROTTLE
        self.setThrottle            = self.SC.setThrottle[self.iterScenario]

    def run(self):

        if self.FM.moveType == 0:   # DIRECT FLIGHT

            if self.stopConstraint == 1: # MACH CONSTRAINT

                if self.FM.speedType == 1:  # ACCELERATION
                    if self.stopValue >= self.FM.mach:
                        return True
                if self.FM.speedType == -1: # DECELERATION
                    if self.stopValue < self.FM.mach:
                        return True

            if self.stopConstraint == 2: # FUEL CONSTRAINT

                if self.FM.speedType == 1:  # ACCELERATION
                    if self.stopValue < self.AC.fuelFraction:
                        return True
                if self.FM.speedType == -1: # DECELERATION
                    if self.stopValue < self.AC.fuelFraction:
                        return True
                if self.FM.speedType == 0: # CONSTANT SPEED
                    if self.stopValue < self.AC.fuelFraction:
                        return True

            if self.stopConstraint == 3:        # ANGLE CONSTRAINT
                pass

            if self.stopConstraint == 4:        # ALTITUDE CONSTRAINT

                if self.FM.k < 0:
                    if self.FM.speedType == 1:  # ACCELERATION
                        if self.stopValue >= self.FM.altitude:
                            return True
                    if self.FM.speedType == -1:  # DECELERATION
                        if self.stopValue >= self.FM.altitude:
                            return True
                    if self.FM.speedType == 0:  # CONSTANT SPEED
                        if self.stopValue >= self.FM.altitude:
                            return True

                if self.FM.k >= 0:
                    if self.FM.speedType == 1:  # ACCELERATION
                        if self.stopValue < self.FM.altitude:
                            return True
                    if self.FM.speedType == -1:  # DECELERATION
                        if self.stopValue < self.FM.altitude:
                            return True
                    if self.FM.speedType == 0:  # CONSTANT SPEED
                        if self.stopValue < self.FM.altitude:
                            return True

        elif self.FM.moveType == 1:   # NOSE UP

            if self.stopConstraint == 1: # MACH CONSTRAINT

                if self.FM.speedType == 1:  # ACCELERATION
                    if self.stopValue >= self.FM.mach:
                        return True
                if self.FM.speedType == -1: # DECELERATION
                    if self.stopValue < self.FM.mach:
                        return True

            if self.stopConstraint == 2: # FUEL CONSTRAINT

                if self.FM.speedType == 1:  # ACCELERATION
                    if self.stopValue < self.AC.fuelFraction:
                        return True
                if self.FM.speedType == -1: # DECELERATION
                    if self.stopValue < self.AC.fuelFraction:
                        return True
                if self.FM.speedType == 0: # CONSTANT SPEED
                    if self.stopValue < self.AC.fuelFraction:
                        return True

            if self.stopConstraint == 3: # ANGLE CONSTRAINT

                if self.FM.speedType == 1:  # ACCELERATION
                    if - self.stopValue < np.degrees(np.arcsin(self.FM.k)):
                        return True
                if self.FM.speedType == -1: # DECELERATION
                    if - self.stopValue < np.degrees(np.arcsin(self.FM.k)):
                        return True
                if self.FM.speedType == 0: # CONSTANT SPEED
                    if - self.stopValue < np.degrees(np.arcsin(self.FM.k)):
                        return True

            if self.stopConstraint == 4: # ALTITUDE CONSTRAINT
                pass

        elif self.FM.moveType == -1:   # NOSE DOWN

            if self.stopConstraint == 1: # MACH CONSTRAINT

                if self.FM.speedType == 1:  # ACCELERATION
                    if self.stopValue >= self.FM.mach:
                        return True
                if self.FM.speedType == -1: # DECELERATION
                    if self.stopValue < self.FM.mach:
                        return True

            if self.stopConstraint == 2: # FUEL CONSTRAINT

                if self.FM.speedType == 1:  # ACCELERATION
                    if self.stopValue < self.AC.fuelFraction:
                        return True
                if self.FM.speedType == -1: # DECELERATION
                    if self.stopValue < self.AC.fuelFraction:
                        return True
                if self.FM.speedType == 0: # CONSTANT SPEED
                    if self.stopValue < self.AC.fuelFraction:
                        return True

            if self.stopConstraint == 3: # ANGLE CONSTRAINT

                if self.FM.speedType == 1:  # ACCELERATION
                    if self.stopValue >= np.degrees(np.arcsin(self.FM.k)):
                        return True
                if self.FM.speedType == -1: # DECELERATION
                    if self.stopValue >= np.degrees(np.arcsin(self.FM.k)):
                        return True
                if self.FM.speedType == 0: # CONSTANT SPEED
                    if self.stopValue >= np.degrees(np.arcsin(self.FM.k)):
                        return True

            if self.stopConstraint == 4: # ALTITUDE CONSTRAINT
                pass

        elif self.FM.moveType == 2:

            # if self.stopConstraint == 1: # MACH CONSTRAINT
            #
            #     if self.FM.speedType == 1:  # ACCELERATION
            #         if self.stopValue >= self.FM.mach:
            #             return True
            #     if self.FM.speedType == -1: # DECELERATION
            #         if self.stopValue < self.FM.mach:
            #             return True
            #
            # if self.stopConstraint == 2: # FUEL CONSTRAINT
            #
            #     if self.FM.speedType == 1:  # ACCELERATION
            #         if self.stopValue < self.AC.fuelFraction:
            #             return True
            #     if self.FM.speedType == -1: # DECELERATION
            #         if self.stopValue < self.AC.fuelFraction:
            #             return True
            #     if self.FM.speedType == 0: # CONSTANT SPEED
            #         if self.stopValue < self.AC.fuelFraction:
            #             return True
            #
            # if self.stopConstraint == 3: # ANGLE CONSTRAINT
            #
            #     if self.FM.speedType == 1:  # ACCELERATION
            #         if self.stopValue >= np.degrees(np.arcsin(self.FM.k)):
            #             return True
            #     if self.FM.speedType == -1: # DECELERATION
            #         if self.stopValue >= np.degrees(np.arcsin(self.FM.k)):
            #             return True
            #     if self.FM.speedType == 0: # CONSTANT SPEED
            #         if self.stopValue >= np.degrees(np.arcsin(self.FM.k)):
            #             return True
            #
            # if self.stopConstraint == 4: # ALTITUDE CONSTRAINT
            #     pass

            if self.stopConstraint == 5: # TURN CONSTRAINT

                if self.stopValue   == "half" and self.previousDirectionI > 0 and self.previousDirectionJ <= 0:
                    if self.FM.speedType == 1:  # ACCELERATION
                        if self.FM.j <= 0:
                            return True
                    if self.FM.speedType == -1: # DECELERATION
                        if self.FM.j <= 0:
                            return True
                    if self.FM.speedType == 0: # CONSTANT SPEED
                        if self.FM.j <= 0:
                            return True

                if self.stopValue   == "half" and self.previousDirectionI < 0 and self.previousDirectionJ >= 0:
                    if self.FM.speedType == 1:  # ACCELERATION
                        if self.FM.j >= 0:
                            return True
                    if self.FM.speedType == -1: # DECELERATION
                        if self.FM.j >= 0:
                            return True
                    if self.FM.speedType == 0: # CONSTANT SPEED
                        if self.FM.j >= 0:
                            return True

        return False

    def next(self):

        self.iterScenario   += 1

        if self.iterScenario == len(self.SC.moveType):

            return False

        if self.SC.moveType[self.iterScenario] == 2:    # Keep previous direction to track quarter, half or full turn

            self.previousDirectionI     = self.FM.i
            self.previousDirectionJ     = self.FM.j
            self.previousDirectionK     = self.FM.k

        return True

    def set(self):

        # SET SOLVER STRUCTURE
        if self.SC.speedType[self.iterScenario] == 0:
            self.structure          = 1
            self.FM.speedType       = 0
        elif self.SC.speedType[self.iterScenario]   == 1:
            self.structure          = 2
            self.FM.speedType       = 1
        else:
            self.structure          = 2
            self.FM.speedType       = -1

        # SET PILOT INPUT
        if self.SC.moveType[self.iterScenario]      == -1:
            self.FM.moveType           = -1
        elif self.SC.moveType[self.iterScenario]    == 0:
            self.FM.moveType           = 0
        elif self.SC.moveType[self.iterScenario]    == 1:
            self.FM.moveType           = 1
        else:
            self.FM.moveType           = 2

        # SET STOP TYPE
        if self.SC.constraint[self.iterScenario]    == 1:
            self.stopConstraint     = 1
        elif self.SC.constraint[self.iterScenario]  == 2:
            self.stopConstraint     = 2
        elif self.SC.constraint[self.iterScenario]  == 3:
            self.stopConstraint     = 3
        elif self.SC.constraint[self.iterScenario]  == 4:
            self.stopConstraint     = 4
        else:
            self.stopConstraint     = 5


        # SET STOP VALUE
        self.stopValue              = self.SC.value[self.iterScenario]

        # SET THROTTLE
        self.setThrottle            = self.SC.setThrottle[self.iterScenario]

    def flight(self):

        self.DB.dragForce(self.FM.mach, self.AC.totalWeight, self.AP.density)

        if self.structure   == 1:

            self.EN.fixedVelocity(self.DB.currentDragForce, self.AP.altitude, self.FM.flightSpeed)
            self.FM.nextTime(self.AC, self.AP, self.DB.currentDragForce, self.EN.currentThrust)

        elif self.structure == 2:

            if self.setThrottle == "full":
                self.EN.fullThrottle(self.AP.altitude, self.FM.flightSpeed)
            else:
                self.EN.fixedThrottle(self.setThrottle, self.AP.altitude, self.FM.flightSpeed)

            self.FM.nextMach(self.AC, self.AP, self.DB.currentDragForce, self.EN.currentThrust)

        self.AP.update(self.FM.altitude)
        self.AC.update(self.EN.fuel * self.FM.time)

        self.results["x"].append(self.FM.x)
        self.results["y"].append(- self.FM.y)
        self.results["z"].append(self.FM.altitude)

        self.results["i"].append(self.FM.i)
        self.results["j"].append(self.FM.j)
        self.results["k"].append(self.FM.k)

        self.results["mach"].append(self.FM.mach)
        self.results["time"].append(self.FM.flightTime)

        self.results["fuelFraction"].append(self.AC.fuelFraction * 100)

        self.results["dbDrag"].append(self.DB.currentDragForce)
        self.results["thrust"].append(self.EN.currentThrust)
        self.results["acDrag"].append(self.FM.drag)
        self.results["netForce"].append(self.FM.net)

        self.results["throttle"].append(self.EN.throttle * 100)
        self.results["fuelRate"].append(self.EN.fuel)

    def stop(self): # Inevitable Stop Conditions

        if self.FM.altitude < 0:
            print("***************************\nALTITUDE IS BELOW SEA LEVEL\n***************************")
            return True
        elif self.AC.fuelFraction < 0:
            print("***************************\nEMPTY FUEL\n***************************")
            return True
        elif self.FM.speedType == 1 and self.FM.net < 0:
            print("***************************\nNEGATIVE NET FORCE\n***************************")
            return True
        elif self.FM.speedType == -1 and self.FM.net > 0:
            print("***************************\nPOSITIVE NET FORCE\n***************************")
            return True

        return False

class Visual:   # v.1.1

    class Database(object):
        pass

    class Engine(object):
        pass

    class Simulation(object):

        def __init__(self):
            pass

        def plot(self, results):

            self.x              = results["x"]
            self.y              = results["y"]
            self.z              = results["z"]

            self.i              = results["i"]
            self.j              = results["j"]
            self.k              = results["k"]

            self.mach           = results["mach"]
            self.time           = results["time"]

            self.fuelFraction   = results["fuelFraction"]

            self.dbDrag         = results["dbDrag"]
            self.thrust         = results["thrust"]
            self.acDrag         = results["acDrag"]
            self.netForce       = results["netForce"]

            self.fuelRate       = results["fuelRate"]
            self.throttle       = results["throttle"]

            # Plot Flight Path
            ax                  = plt.axes(projection='3d')
            for i in range(len(self.x) - 1):
                ax.plot(self.x[i:i+2], self.y[i:i+2], self.z[i:i+2], color=plt.cm.jet(i/(len(self.y) - 1)))

            # Plot Fuel Fraction
            plt.figure()
            plt.plot(self.time, self.fuelFraction)
            plt.title('Fuel Consumption during Flight')
            plt.xlabel('Flight Time (sec)')
            plt.ylabel('Fuel Left (%)')
            plt.grid()

            # Plot Mach
            plt.figure()
            plt.plot(self.time[:(len(self.time) - 1)], self.mach[:(len(self.time) - 1)])
            plt.title('Aircraft Speed during Flight')
            plt.xlabel('Flight Time (sec)')
            plt.ylabel('Mach')
            plt.grid()

            # Plot i, j, k
            fig, axs    = plt.subplots(3)
            fig.suptitle('Aircraft Direction during Flight')
            axs[0].plot(self.time, self.i)
            axs[0].grid()
            axs[0].set(ylabel='i')
            axs[1].plot(self.time, self.j)
            axs[1].grid()
            axs[1].set(ylabel='j')
            axs[2].plot(self.time, self.k)
            axs[2].grid()
            axs[2].set(ylabel='k', xlabel='Flight Time (sec)')

            # Plot Aerodynamic Forces over Time
            fig, axs    = plt.subplots(2, 2)
            fig.suptitle('Aerodynamic Parameters during Flight')
            axs[0][0].plot(self.time[:(len(self.time) - 1)], self.dbDrag[:(len(self.time) - 1)])
            axs[0][0].grid()
            axs[0][0].set(ylabel='Database Drag (N)')

            axs[1][0].plot(self.time[:(len(self.time) - 1)], self.acDrag[:(len(self.time) - 1)])
            axs[1][0].grid()
            axs[1][0].set(xlabel='Flight Time (sec)', ylabel='Aircraft Drag (N)')

            axs[0][1].plot(self.time[:(len(self.time) - 1)], self.thrust[:(len(self.time) - 1)])
            axs[0][1].grid()
            axs[0][1].set(ylabel='Thrust (N)')

            axs[1][1].plot(self.time[:(len(self.time) - 1)], self.netForce[:(len(self.time) - 1)])
            axs[1][1].grid()
            axs[1][1].set(xlabel='Flight Time (sec)', ylabel='Net Force (N)')

            # Plot Engine Props
            fig, axs    = plt.subplots(2)
            fig.suptitle('Engine Parameters during Flight')
            axs[0].plot(self.time, self.fuelRate)
            axs[0].grid()
            axs[0].set(ylabel='Fuel Rate (kg/sec)')

            axs[1].plot(self.time, self.throttle)
            axs[1].grid()
            axs[1].set(xlabel='Flight Time (sec)', ylabel='Throttle (%)')

            # Plot Show
            plt.show()

#####################################################
def globalVars():
    #####################################################
    global direct
    global noseUp
    global noseDown
    global turn
    #####################################################
    direct          = 0
    noseUp          = 1
    noseDown        = -1
    turn            = 2
    #####################################################
    global constantSpeed
    global accelerate
    global decelerate
    #####################################################
    constantSpeed   = 0
    accelerate      = 1
    decelerate      = -1
    #####################################################
    global machConstraint
    global fuelConstraint
    global angleConstraint
    global altConstraint
    global turnConstraint
    #####################################################
    machConstraint  = 1
    fuelConstraint  = 2
    angleConstraint = 3
    altConstraint   = 4
    turnConstraint  = 5
    #####################################################
#####################################################
# MAIN LOOP
def main():
    while True:
        driver.flight()
        if driver.stop():
            visual.plot(driver.results)
            break
        if not driver.run():
            if driver.next():
                driver.set()
            else:
                visual.plot(driver.results)
                break
#####################################################
globalVars()
#####################################################
# scenario
# move # noseDown, direct, noseUp, turn
moveType        = [direct, noseUp, direct, noseDown, turn, direct, direct]
# speed # decelerate, constantSpeed, accelerate
speedType       = [accelerate, constantSpeed, constantSpeed, constantSpeed, constantSpeed, accelerate, constantSpeed]
# constraint # machConstraint, fuelConstraint, angleConstraint, altConstraint, turnConstraint
constraint      = [machConstraint, angleConstraint, altConstraint, angleConstraint, turnConstraint, machConstraint, fuelConstraint]
# value # constraint stop value
value           = [0.5, 5., 4500, 0., "half", 0.65, 0.2]
# throttle settings # float: acceleration / deceleration, "set": constantSpeed, "full": full throttle
setThrottle     = [0.75, "set", "set", "set", "set", "full", "set"]
#####################################################
# initial conditions
startMach       = 0.2
machIncrement   = 0.001
timeIncrement   = 0.1
#####################################################
# aircraft properties
totalMass       = 31.
fuelMass        = 6.
planformArea    = 0.165
maxG            = 3.
intakeRatio     = 2.9
#####################################################
# atmosphere properties
ISA             = 0.
startAltitude   = 3000.
gamma           = 1.4
R               = 287.
#####################################################
# required file paths
enginePath      = 'engine\\engine_lookUp.dat'
databasePath    = 'database\\database_lookUp.dat'
databaseRho     = 0.77
#####################################################

############################################----------------############################################################

########################################################################################################################
############################################----------------############################################################
########################################################################################################################
initialConditionsDict   = {"initialMach": startMach, "machIncrement": machIncrement, "timeIncrement": timeIncrement}
stopConditionsDict      = {"move": moveType, "speed": speedType, "constraint": constraint, "value": value, "throttle": setThrottle}
aircraftDict            = {"Mass": totalMass, "fuelMass": fuelMass, "planformArea": planformArea, "maxG": maxG, "intakeAreaRatio": intakeRatio}
atmosphereDict          = {"ISA": ISA, "altitude": startAltitude, "gamma": gamma, "R": R}
########################################################################################################################
aircraft                = Vehicle.Aircraft(aircraftDict)
engine                  = Vehicle.Engine.LookUpTable(enginePath, aircraft.intakeAreaRatio)
aerodynamic             = Vehicle.Database.LookUpTable(databasePath, databaseRho)
########################################################################################################################
atmosphere              = Conditions.AtmosphericConditions(atmosphereDict)
initial                 = Conditions.InitialConditions(initialConditionsDict)
stop                    = Conditions.StopConditions(stopConditionsDict)
########################################################################################################################
flightMech              = FlightMechanics(aircraft, atmosphere, initial)
########################################################################################################################
driver                  = Driver(aircraft, engine, aerodynamic, atmosphere, flightMech, initial, stop)
########################################################################################################################
visual                  = Visual.Simulation()
########################################################################################################################
############################################----------------############################################################
########################################################################################################################

if __name__ == '__main__':
    main()