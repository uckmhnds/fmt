import numpy as np
import os
from scipy import interpolate
import matplotlib.pyplot as plt

with open(os.path.join(os.path.dirname(os.path.realpath("__file__")), "engine_lookUp.dat"), "r") as output:
    outputLines     = output.readlines()
    outputWords     = [outputLines[i].split() for i in range(len(outputLines))]
    alt1             = [float(outputWords[i][0]) for i in range(len(outputWords)) if (i%2) == 0]
    alt2             = [float(outputWords[i][0]) for i in range(len(outputWords)) if (i%2) == 1]
    thr1            = [float(outputWords[i][1]) for i in range(len(outputWords))if (i%2) == 0]
    thr2            = [float(outputWords[i][1]) for i in range(len(outputWords))if (i%2) == 1]
    fuel1           = [float(outputWords[i][2]) for i in range(len(outputWords))if (i%2) == 0]
    fuel2           = [float(outputWords[i][2]) for i in range(len(outputWords))if (i%2) == 1]
    f1              = [float(outputWords[i][3]) for i in range(len(outputWords))if (i%2) == 0]
    f2              = [float(outputWords[i][3]) for i in range(len(outputWords))if (i%2) == 1]
    v1              = [float(outputWords[i][4]) for i in range(len(outputWords))if (i%2) == 0]
    v2              = [float(outputWords[i][4]) for i in range(len(outputWords))if (i%2) == 1]
    m1              = [float(outputWords[i][5]) for i in range(len(outputWords))if (i%2) == 0]
    m2              = [float(outputWords[i][5]) for i in range(len(outputWords))if (i%2) == 1]

throttle                = 1.
flightVelocity          = 0.
intakeAreaRatio         = 1.
altitude                = 3000.

fuels                   = []
engineForces            = []
exhaustVelocities       = []
airflowRates            = []
thrusts                 = []

for i in range(len(alt1)):

    altitudeValues          = [alt1[i],     alt2[i]]
    fuelValues              = [fuel1[i],    fuel2[i]]
    engineForceValues       = [f1[i],       f2[i]]
    exhaustVelocityValues   = [v1[i],       v2[i]]
    airFlowValues           = [m1[i],       m2[i]]

    if altitude > alt2[i]:

        interpolateFuel     = interpolate.interp1d(altitudeValues, fuelValues, fill_value='extrapolate')
        interpolateForce    = interpolate.interp1d(altitudeValues, engineForceValues, fill_value='extrapolate')
        interpolateVelocity = interpolate.interp1d(altitudeValues, exhaustVelocityValues, fill_value='extrapolate')
        interpolateAir      = interpolate.interp1d(altitudeValues, airFlowValues, fill_value='extrapolate')

    else:

        interpolateFuel     = interpolate.interp1d(altitudeValues, fuelValues, fill_value='interpolate')
        interpolateForce    = interpolate.interp1d(altitudeValues, engineForceValues, fill_value='interpolate')
        interpolateVelocity = interpolate.interp1d(altitudeValues, exhaustVelocityValues, fill_value='interpolate')
        interpolateAir      = interpolate.interp1d(altitudeValues, airFlowValues, fill_value='interpolate')

    fuels.append(interpolateFuel(altitude))  # kg / sec
    engineForces.append(interpolateForce(altitude))  # N
    exhaustVelocities.append(interpolateVelocity(altitude))  # m / sec
    airflowRates.append(interpolateAir(altitude))  # kg / sec

    thrusts.append(interpolateAir(altitude) * (interpolateVelocity(altitude) - flightVelocity / intakeAreaRatio))


fig, (axs1, axs2)    = plt.subplots(1, 2)
axs1.plot(thr1, f1, "o", color='red', label='seaLevel')
axs1.plot(thr2, f2, "o", color='blue', label='15k feet')
axs1.plot(thr1, engineForces, color='black', label='interpolated')
axs1.legend(loc='upper left')
axs1.set_title('Engine Thrust Points and Interpolated Values')
axs1.set_xlabel('Throttle')
axs1.set_ylabel('Thrust (N)')
axs1.grid()

axs2.plot(thr1, fuel1, "o", color='red', label='seaLevel')
axs2.plot(thr2, fuel2, "o", color='blue', label='15k feet')
axs2.plot(thr1, fuels, color='black', label='interpolated')
axs2.legend(loc='upper left')
axs2.set_title('Engine Fuel Points and Interpolated Values')
axs2.set_xlabel('Throttle')
axs2.set_ylabel('Fuel Rate (gr / min)')
axs2.grid()

plt.show()