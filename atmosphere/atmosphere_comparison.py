import numpy as np
import os
import matplotlib.pyplot as plt

list            = np.arange(0., 10000., 1.)

with open(os.path.join(os.path.dirname(os.path.realpath("__file__")), "atmosphere_formulaISA0.dat"), "r") as output:

    outputLines     = output.readlines()
    outputWords     = [outputLines[i].split() for i in range(len(outputLines))]

    temperature0    = [float(outputWords[i][1]) for i in range(len(outputWords))]
    pressure0       = [float(outputWords[i][2]) for i in range(len(outputWords))]
    density0        = [float(outputWords[i][3]) for i in range(len(outputWords))]
    sound0          = [float(outputWords[i][4]) for i in range(len(outputWords))]

# fig, axs    = plt.subplots(2, 2)
# fig.suptitle('Atmospheric Properties')
# axs[0, 0].plot(list, temperature0)
# axs[0, 0].set_title('Temperature')
# axs[0, 0].grid()
# axs[0, 1].plot(list, pressure0)
# axs[0, 1].set_title('Pressure')
# axs[0, 1].grid()
# axs[1, 0].plot(list, density0)
# axs[1, 0].set_title('Density')
# axs[1, 0].grid()
# axs[1, 1].plot(list, sound0)
# axs[1, 1].set_title('Speed of Sound')
# axs[1, 1].grid()

fig, (axs1, axs2)    = plt.subplots(1, 2)

axs1.plot(list, sound0)
axs1.set_title('Speed of Sound vs Altitude')
axs1.set_xlabel('Altitude (m)')
axs1.set_ylabel('Speed of Sound (m/s)')
axs1.grid()

axs2.plot(list, density0)
axs2.set_title('Density vs Altitude')
axs2.set_xlabel('Altitude (m)')
axs2.set_ylabel('Density (kg/m3)')
axs2.grid()

plt.show()