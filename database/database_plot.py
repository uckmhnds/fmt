import numpy as np
import os
from scipy import interpolate
import matplotlib.pyplot as plt

with open(os.path.join(os.path.dirname(os.path.realpath("__file__")), "database_lookUp.dat"), "r") as output:
    outputLines     = output.readlines()
    outputWords     = [outputLines[i].split() for i in range(len(outputLines))]
    mach            = [float(outputWords[i][0]) for i in range(len(outputWords)) if (i%2) == 0]
    alpha1           = [float(outputWords[i][2]) for i in range(len(outputWords))if (i%2) == 0]
    alpha2           = [float(outputWords[i][2]) for i in range(len(outputWords))if (i%2) == 1]
    fx1              = [float(outputWords[i][3]) for i in range(len(outputWords))if (i%2) == 0]
    fx2              = [float(outputWords[i][3]) for i in range(len(outputWords))if (i%2) == 1]
    fz1              = [float(outputWords[i][5]) for i in range(len(outputWords))if (i%2) == 0]
    fz2              = [float(outputWords[i][5]) for i in range(len(outputWords))if (i%2) == 1]
    cx1              = [float(outputWords[i][9]) for i in range(len(outputWords))if (i%2) == 0]
    cx2              = [float(outputWords[i][9]) for i in range(len(outputWords))if (i%2) == 1]
    cz1              = [float(outputWords[i][11]) for i in range(len(outputWords))if (i%2) == 0]
    cz2              = [float(outputWords[i][11]) for i in range(len(outputWords))if (i%2) == 1]

lift1    = fz1 * np.cos(np.radians(alpha1)) - fx1 * np.sin(np.radians(alpha1))
lift2    = fz2 * np.cos(np.radians(alpha2)) - fx2 * np.sin(np.radians(alpha2))
drag1    = fz1 * np.sin(np.radians(alpha1)) + fx1 * np.cos(np.radians(alpha1))
drag2    = fz2 * np.sin(np.radians(alpha2)) + fx2 * np.cos(np.radians(alpha2))

cl1     = cz1 * np.cos(np.radians(alpha1)) - cx1 * np.sin(np.radians(alpha1))
cl2     = cz2 * np.cos(np.radians(alpha2)) - cx2 * np.sin(np.radians(alpha2))
cd1     = cz1 * np.sin(np.radians(alpha1)) + cx1 * np.cos(np.radians(alpha1))
cd2     = cz2 * np.sin(np.radians(alpha2)) + cx2 * np.cos(np.radians(alpha2))

print(cl1)
print(cl2)
print(cd1)
print(cd2)


# interpolatedDrag    = []
# interpolatedAlpha   = []
#
# aircraftWeight      = 300
# for i in range(len(lift1)):
#
#     lift_values     = [lift1[i], lift2[i]]
#     drag_values     = [drag1[i], drag2[i]]
#     alpha_values    = [alpha1[i], alpha2[i]]
#
#     if aircraftWeight < lift1[i] or aircraftWeight > lift2[i]:
#
#         interpolationFunc   = interpolate.interp1d(lift_values, drag_values, fill_value='extrapolate')
#         interpolationFunc2  = interpolate.interp1d(lift_values, alpha_values, fill_value='extrapolate')
#
#     else:
#
#         interpolationFunc = interpolate.interp1d(lift_values, drag_values, fill_value='interpolate')
#         interpolationFunc2  = interpolate.interp1d(lift_values, alpha_values, fill_value='interpolate')
#
#     drag    = interpolationFunc(aircraftWeight)
#     alpha   = interpolationFunc2(aircraftWeight)
#
#     interpolatedAlpha.append(alpha)
#     interpolatedDrag.append(drag)
#
# MachList        = np.arange(0.2, 1.1, 0.01)
# dragList        = []
# alphaList       = []
#
# for i in range(len(MachList)):
#     if MachList[i] < mach[0] or MachList[i] > mach[len(mach) - 1]:
#
#         interpolationFunc = interpolate.interp1d(mach, interpolatedDrag, fill_value='extrapolate')
#         interpolationFunc2= interpolate.interp1d(mach, interpolatedAlpha, fill_value='extrapolate')
#
#     else:
#
#         interpolationFunc = interpolate.interp1d(mach, interpolatedDrag, fill_value='interpolate')
#         interpolationFunc2= interpolate.interp1d(mach, interpolatedAlpha, fill_value='interpolate')
#
#     dragList.append(interpolationFunc(MachList[i]))
#     alphaList.append(interpolationFunc2(MachList[i]))
#
# fig, (axs1, axs2)    = plt.subplots(1, 2)
# axs1.plot(mach, drag1, "o", color='red', label='lower')
# axs1.plot(mach, drag2, "o", color='blue', label='upper')
# axs1.plot(MachList, dragList)
# axs1.legend(loc='upper left')
# axs1.set_title('Interpolated Drag')
# axs1.set_xlabel('Mach')
# axs1.set_ylabel('Drag (N)')
# axs1.grid()
#
# axs2.plot(mach, alpha1, "o", color='red', label='lower')
# axs2.plot(mach, alpha2, "o", color='blue', label='upper')
# axs2.plot(MachList, alphaList)
# axs2.legend(loc='upper right')
# axs2.set_title('Interpolated Angle of Attack')
# axs2.set_xlabel('Mach')
# axs2.set_ylabel('AoA (deg)')
# axs2.grid()
#
# plt.show()